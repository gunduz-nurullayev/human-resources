/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.shaheen.nazarov.human.resources.jdbc.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.shaheen.nazarov.human.resources.access.tool.AbstractTemplate;
import org.shaheen.nazarov.human.resources.jdbc.model.Driver;
import org.shaheen.nazarov.human.resources.jdbc.service.*;
import org.shaheen.nazarov.human.resources.jdbc.util.SearchModel;
import org.springframework.jdbc.core.BeanPropertyRowMapper;

/**
 *
 * @author Shahin
 */
public class DriverServiceImpl extends AbstractTemplate implements DriverService {

    @Override
    public void save(Driver model) {
        getTemplate().update("insert into Drivers(Name,Surname,Father_Name) values (?,?,?)", new Object[]{model.getName(), model.getSurName(), model.getFatherName()});
    }

    @Override
    public void update(Driver model) {
        getTemplate().update("update Drivers set Name=?,Surname=?,Father_Name=? where id=?", new Object[]{model.getName(), model.getSurName(), model.getFatherName(), model.getId()});
    }

    @Override
    public void delete(Long key) {
        getTemplate().update("delete from Drivers  where id=?", new Object[]{key});
    }

    @Override
    public Driver getByPK(Long key) {
        return (Driver) getTemplate().queryForObject("select * from Drivers where id=?", new Object[]{key}, new BeanPropertyRowMapper(Driver.class));
    }

    @Override
    public List<Driver> getList() {
        return getTemplate().query("select * from Drivers ", new BeanPropertyRowMapper(Driver.class));
    }

    @Override
    public List<Driver> findDriver(SearchModel model) {
        List<Object> objects = new ArrayList<>();
        String sql = "SELECT * FROM (Drivers INNER JOIN Cars ON Drivers.ID = Cars.Driver_Id) INNER JOIN Econom ON Cars.ID = Econom.Car_Id ";
        int size = sql.length();
        if (model.getName()!=null) {
            sql += (size == sql.length() ? " where Drivers.Name Like ? " : " or Drivers.Name Like ?  ");
            objects.add("%"+model.getName()+"%");
        }
        if (model.getSurName()!=null) {
            sql += (size == sql.length() ? " where Drivers.SurName Like ? " : " or Drivers.SurName Like ? ");
            objects.add("%"+model.getSurName()+"%");
        }
        if (model.getFatherName()!=null) {
            sql += (size == sql.length() ? " where Drivers.Father_Name Like ? " : " or Drivers.Father_Name Like ? ");
            objects.add("%"+model.getFatherName()+"%");
        }
        if (model.getCarId() != 0) {
            sql += (size == sql.length() ? "where Cars.Id=?" : " or Cars.Id=? ");
            objects.add(model.getCarId());
        }
        if (model.isSort()) {
            sql += " order by Econom.econom ASC";
        } else {
            sql += " order by Econom.econom DESC";
        }
        System.out.println(sql);
        return getTemplate().query(sql, objects.toArray(), new BeanPropertyRowMapper(Driver.class));
    }

}
