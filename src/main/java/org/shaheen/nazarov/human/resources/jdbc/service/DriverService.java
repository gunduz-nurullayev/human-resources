
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.shaheen.nazarov.human.resources.jdbc.service;

import java.util.List;
import org.shaheen.nazarov.human.resources.access.tool.AbstractDao;
import org.shaheen.nazarov.human.resources.jdbc.model.Driver;
import org.shaheen.nazarov.human.resources.jdbc.util.SearchModel;
/**
 *
 * @author Shahin
 */
public interface DriverService   extends AbstractDao<Long,Driver>{
    List<Driver> findDriver(SearchModel model);
}
