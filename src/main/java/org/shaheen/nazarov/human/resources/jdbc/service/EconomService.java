/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.shaheen.nazarov.human.resources.jdbc.service;

import org.shaheen.nazarov.human.resources.access.tool.AbstractDao;
import org.shaheen.nazarov.human.resources.jdbc.model.Econom;

/**
 *
 * @author Shahin
 */
public interface EconomService  extends AbstractDao<Long,Econom>{
      public Econom getEconomByCar(Long id);  
}
