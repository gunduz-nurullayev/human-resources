/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.shaheen.nazarov.human.resources.access.tool;

import java.util.List;
import java.io.Serializable;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Shahin
 * @param <PK>
 * @param <C>
 */
public interface AbstractDao<PK extends Serializable , C>{
    @Transactional
    public void save(C model);
    @Transactional
    public void update(C model);
    @Transactional
    public void delete(PK key);
    @Transactional(readOnly = true)
    public C getByPK(PK key);
    @Transactional(readOnly = true)
    public List<C> getList();
}
