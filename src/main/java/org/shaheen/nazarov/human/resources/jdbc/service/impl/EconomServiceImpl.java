/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.shaheen.nazarov.human.resources.jdbc.service.impl;

import java.util.List;
import org.shaheen.nazarov.human.resources.access.tool.AbstractTemplate;
import org.shaheen.nazarov.human.resources.jdbc.model.Econom;
import org.shaheen.nazarov.human.resources.jdbc.service.EconomService;
import org.springframework.jdbc.core.BeanPropertyRowMapper;

/**
 *
 * @author Shahin
 */
public class EconomServiceImpl extends AbstractTemplate implements EconomService {

    @Override
    public void save(Econom model) {
        model.setEconom(model.getReal() - model.getEconom());
        getTemplate().update("insert into Econom(Real,Actual,Econom,Car_Id) values (?,?,?,?)", new Object[]{
            model.getReal(), model.getActual(), model.getEconom(), model.getCarId()
        });
    }

    @Override
    public void update(Econom model) {
        model.setEconom(model.getReal() - model.getEconom());
        getTemplate().update("update Econom set Real=?,Actual=?,Econom=?,Car_Id=? where id=?", new Object[]{
            model.getReal(), model.getActual(), model.getEconom(), model.getCarId(), model.getId()
        });
    }

    @Override
    public void delete(Long key) {
        getTemplate().update("delete from Econom  where id=?", new Object[]{key});
    }

    @Override
    public Econom getByPK(Long key) {
        return (Econom) getTemplate().queryForObject("select * from Econom  where id=?", new Object[]{key}, new BeanPropertyRowMapper(Econom.class));
    }

    @Override
    public List<Econom> getList() {
        return getTemplate().query("select * from Econom", new BeanPropertyRowMapper(Econom.class));
    }

    @Override
    public Econom getEconomByCar(Long id) {
        List<Econom> economs = getTemplate().query("select * from Econom  where car_id=?", new Object[]{id}, new BeanPropertyRowMapper(Econom.class));
        return economs.isEmpty() ? null : economs.get(0);
    }

}
