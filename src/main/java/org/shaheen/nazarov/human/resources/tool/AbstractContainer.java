/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.shaheen.nazarov.human.resources.tool;

import java.sql.SQLException;
import org.shaheen.nazarov.human.resources.access.tool.AbstractTemplate;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author Shahin
 */
public class AbstractContainer {
    
    private static ApplicationContext applicationContext;
    
    private AbstractContainer(){}
    
    public static ApplicationContext getContext(){
        if(applicationContext == null){
            applicationContext = new ClassPathXmlApplicationContext("spring.xml");
        }
        return applicationContext;
    }
    
}
