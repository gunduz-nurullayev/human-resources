/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.shaheen.nazarov.human.resources.jdbc.service.impl;

import java.util.List;
import org.shaheen.nazarov.human.resources.access.tool.AbstractTemplate;
import org.shaheen.nazarov.human.resources.jdbc.model.Car;
import org.shaheen.nazarov.human.resources.jdbc.service.*;
import org.springframework.jdbc.core.BeanPropertyRowMapper;

/**
 *
 * @author Shahin
 */
public class CarServiceImpl extends AbstractTemplate implements CarService {

    @Override
    public void save(Car model) {
        getTemplate().update("insert into Cars(Mark,Number,Km,Driver_Id) values (?,?,?,?)", new Object[]{model.getMark(), model.getNumber(), model.getKm(), model.getDriverId()});
    }

    @Override
    public void update(Car model) {
        getTemplate().update("update Cars set Mark=?,Number=?,Km=?,Driver_Id=? where id=?", new Object[]{model.getMark(), model.getNumber(), model.getKm(), model.getDriverId(), model.getId()});
    }

    @Override
    public void delete(Long key) {
        getTemplate().update("delete from Cars  where id=?", new Object[]{key});
    }

    @Override
    public Car getByPK(Long key) {
        return (Car) getTemplate().queryForObject("select * from Cars  where id=?", new Object[]{key}, new BeanPropertyRowMapper(Car.class));
    }

    @Override
    public List<Car> getList() {
        return getTemplate().query("select * from Cars", new BeanPropertyRowMapper(Car.class));
    }

    @Override
    public Car getCarByDriver(Long id) {
        List<Car> cars = getTemplate().query("select * from Cars where Driver_Id=?", new Object[]{id}, new BeanPropertyRowMapper(Car.class));
        return cars.isEmpty() ? null : cars.get(0);
    }

}
