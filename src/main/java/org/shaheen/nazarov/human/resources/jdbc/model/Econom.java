/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.shaheen.nazarov.human.resources.jdbc.model;

import java.sql.Date;

/**
 *
 * @author Shahin
 */
public class Econom {
    
   private long id;
   private double real;
   private double actual;
   private double econom;
   private long carId;

    public long getId() {
        return id;
    }

    public double getEconom() {
        return econom;
    }

    public void setEconom(double econom) {
        this.econom = econom;
    }
    

    public void setId(long id) {
        this.id = id;
    }

    public double getReal() {
        return real;
    }

    public void setReal(double real) {
        this.real = real;
    }

    public double getActual() {
        return actual;
    }

    public void setActual(double actual) {
        this.actual = actual;
    }


    public long getCarId() {
        return carId;
    }

    public void setCarId(long carId) {
        this.carId = carId;
    }

    
}
