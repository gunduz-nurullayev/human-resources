/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.shaheen.nazarov.human.resources.tool;

import java.util.ArrayList;
import java.util.List;
import javax.swing.table.DefaultTableModel;
import org.shaheen.nazarov.human.resources.jdbc.model.Car;
import org.shaheen.nazarov.human.resources.jdbc.model.Driver;
import org.shaheen.nazarov.human.resources.jdbc.model.Econom;
import org.shaheen.nazarov.human.resources.jdbc.service.CarService;
import org.shaheen.nazarov.human.resources.jdbc.service.DriverService;
import org.shaheen.nazarov.human.resources.jdbc.service.EconomService;

/**
 *
 * @author Shahin
 */
public abstract class AbstractTableModel {

    private static final DriverService ds = AbstractContainer.getContext().getBean(DriverService.class);
    private static final CarService cs = AbstractContainer.getContext().getBean(CarService.class);
    private static final EconomService es = AbstractContainer.getContext().getBean(EconomService.class);

    private AbstractTableModel() {

    }

    public static DefaultTableModel allTableModel(List<Driver> drivers) {
        DefaultTableModel model = new DefaultTableModel();
        model.addColumn("Sürücü_ID");
        model.addColumn("Ad");
        model.addColumn("Soyad");
        model.addColumn("Ata Adi");
        model.addColumn("Avtomobil_ID");
        model.addColumn("Marka");
        model.addColumn("Nomrə");
        model.addColumn("Yürüş");
        model.addColumn("Sərfiyyat_ID");
        model.addColumn("Norma");
        model.addColumn("Faktiki");
        model.addColumn("Qənaət");
        for (Driver d : drivers) {
            List<Object> data = new ArrayList();
            data.add(d.getId());
            data.add(d.getName());
            data.add(d.getSurName());
            data.add(d.getFatherName());
            Car c = cs.getCarByDriver(d.getId());
            if (c != null) {
                data.add(c.getId());
                data.add(c.getMark());
                data.add(c.getNumber());
                data.add(c.getKm());
                Econom e = es.getEconomByCar(c.getId());
                if (e != null) {
                    data.add(e.getId());
                    data.add(e.getReal());
                    data.add(e.getActual());
                    data.add(e.getEconom());
                }
            }
            model.addRow(data.toArray());
        }
        return model;
    }

    public static DefaultTableModel driverModel(List<Driver> drivers) {
        DefaultTableModel model = new DefaultTableModel();
        model.addColumn("Sürücü_ID");
        model.addColumn("Ad");
        model.addColumn("Soyad");
        model.addColumn("Ata Adi");
        for (Driver d : drivers) {
            List<Object> data = new ArrayList();
            data.add(d.getId());
            data.add(d.getName());
            data.add(d.getSurName());
            data.add(d.getFatherName());
            model.addRow(data.toArray());
        }
        return model;
    }

    public static DefaultTableModel carModel(List<Car> cars) {
        DefaultTableModel model = new DefaultTableModel();
        model.addColumn("Avtomobil_ID");
        model.addColumn("Marka");
        model.addColumn("Nomrə");
        model.addColumn("Yürüş");
        for (Car c : cars) {
            List<Object> data = new ArrayList();
            data.add(c.getId());
            data.add(c.getMark());
            data.add(c.getNumber());
            data.add(c.getKm());
            model.addRow(data.toArray());
        }
        return model;
    }

    public static DefaultTableModel economModel(List<Econom> economs) {
        DefaultTableModel model = new DefaultTableModel();
        model.addColumn("Sərfiyyat_ID");
        model.addColumn("Norma");
        model.addColumn("Faktiki");
        model.addColumn("Qənaət");
        for (Econom e : economs) {
            List<Object> data = new ArrayList();
            data.add(e.getId());
            data.add(e.getReal());
            data.add(e.getActual());
            data.add(e.getEconom());
            model.addRow(data.toArray());
        }
        return model;
    }

}
