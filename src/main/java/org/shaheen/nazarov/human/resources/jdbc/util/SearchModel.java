/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.shaheen.nazarov.human.resources.jdbc.util;

import org.shaheen.nazarov.human.resources.jdbc.model.Driver;

/**
 *
 * @author Shahin
 */
public class SearchModel extends Driver{
    private long carId;
    private boolean sort;

    public long getCarId() {
        return carId;
    }

    public void setCarId(long carId) {
        this.carId = carId;
    }

    public boolean isSort() {
        return sort;
    }

    public void setSort(boolean sort) {
        this.sort = sort;
    }
    
}
