/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.shaheen.nazarov.human.resources.tool;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.FontSelector;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import javax.swing.table.DefaultTableModel;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

/**
 *
 * @author Shahin
 */
public class ReportCreator {

    public static void createPdf(DefaultTableModel tableModel, String name) throws DocumentException, FileNotFoundException, IOException {
        String fileName = name + (new Date()).toString().replaceAll("/", "-").replaceAll(" ", "-").replaceAll(":", "-") + ".pdf";
        File file = new File(fileName);
        file.createNewFile();
        // step 1
        Document document = new Document();
        // step 2
        PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(file.getAbsolutePath()));
        // step 3:gives error as no suitable method
        writer.getAcroForm().setNeedAppearances(true);

        document.open();
        // step 4
        FontSelector selector = new FontSelector();
        Font font = FontFactory.getFont(FontFactory.TIMES_ROMAN);
        selector.addFont(font);
        font.setColor(BaseColor.BLUE);
        Font f2 = FontFactory.getFont("MSung-Light",
                "UniCNS-UCS2-H", BaseFont.NOT_EMBEDDED);
        f2.setColor(BaseColor.RED);
        selector.addFont(f2);
        
        PdfPTable table = new PdfPTable(tableModel.getColumnCount());
        for (int i = 0; i < tableModel.getColumnCount(); i++) {
            Phrase ph = selector.process(tableModel.getColumnName(i));
            table.addCell(ph);
        }
        
        for (int i = 0; i < tableModel.getRowCount(); i++) {
            for (int j = 0; j < tableModel.getColumnCount(); j++) {
                Phrase ph = selector.process(String.valueOf(tableModel.getValueAt(i, j)));
                table.addCell(ph);
            }
        }
        document.add(table);
        Paragraph paragraph = new Paragraph(new Date().toString() + "  " + name);
        paragraph.setAlignment(Paragraph.ALIGN_CENTER);
        document.add(paragraph);
        // step 5

        document.close();
        Runtime runtime = Runtime.getRuntime();
        try {
            Process proc = runtime.exec("explorer " + file.getAbsolutePath());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void createExcel(DefaultTableModel tableModel, String name) throws IOException {
        String fileName = name + (new Date()).toString().replaceAll("/", "-").replaceAll(" ", "-").replaceAll(":", "-") + ".xls";
        File file = new File(fileName);
        file.createNewFile();
        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheet = workbook.createSheet(name);
        HSSFRow head = sheet.createRow(0);
        for (int i = 0; i < tableModel.getColumnCount(); i++) {
            head.createCell(i).setCellValue(tableModel.getColumnName(i));
        }
        for (int i = 0; i < tableModel.getRowCount(); i++) {
            HSSFRow row = sheet.createRow((i + 1));
            for (int j = 0; j < tableModel.getColumnCount(); j++) {
                row.createCell(j).setCellValue(String.valueOf(tableModel.getValueAt(i, j)));
            }
        }
        FileOutputStream fos = new FileOutputStream(file);
        workbook.write(fos);
        fos.close();
        Runtime runtime = Runtime.getRuntime();
        try {
            Process proc = runtime.exec("explorer " + file.getAbsolutePath());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
